<?php

include 'creds.php';

# Set up variables
$ItemID = filter_var($_POST['ItemID'], FILTER_SANITIZE_STRING);
$ISBN10 = filter_var($_POST['ISBN10'], FILTER_SANITIZE_STRING);
$ISBN13 = filter_var($_POST['ISBN13'], FILTER_SANITIZE_STRING);
$OCLC = filter_var($_POST['OCLC'], FILTER_SANITIZE_STRING);
$ISSN = filter_var($_POST['ISSN'], FILTER_SANITIZE_STRING);
$UPC = filter_var($_POST['UPC'], FILTER_SANITIZE_STRING);
$DOI = filter_var($_POST['DOI'], FILTER_SANITIZE_STRING);

# Debugging output

echo 'ItemID: '.$ItemID.'<br />';
echo 'ISBN10: '.$ISBN10.'<br />';
echo 'ISBN13: '.$ISBN13.'<br />';
echo 'OCLC: '.$OCLC.'<br />';
echo 'ISSN: '.$ISSN.'<br />';
echo 'UPC: '.$UPC.'<br />';
echo 'DOI: '.$DOI.'<br />';

# Set up queries to Update the database

$UpdateISBN10 = "UPDATE ItemRecords SET ISBN10 = '$ISBN10' WHERE ItemRecordID = '$ItemID'";
$UpdateISBN13 = "UPDATE ItemRecords SET ISBN13 = '$ISBN13' WHERE ItemRecordID = '$ItemID'";
$UpdateOCLC = "UPDATE ItemRecords SET OCLC = '$OCLC' WHERE ItemRecordID = '$ItemID'";
$UpdateISSN = "UPDATE ItemRecords SET ISSN = '$ISSN' WHERE ItemRecordID = '$ItemID'";
$UpdateUPC = "UPDATE ItemRecords SET UPC = '$UPC' WHERE ItemRecordID = '$ItemID'";
$UpdateDOI = "UPDATE ItemRecords SET DOI = '$DOI' WHERE ItemRecordID = '$ItemID'";

# Update ISBN10
if (strlen($ISBN10) != 0)
{
    if (mysqli_query($conn, $UpdateISBN10)) {
        echo '<strong>Updated ISBN10</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

# Update ISBN13
if (strlen($ISBN13) != 0)
{
    if (mysqli_query($conn, $UpdateISBN13)) {
        echo '<strong>Updated ISBN13</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

# Update OCLC
if (strlen($OCLC) != 0)
{
    if (mysqli_query($conn, $UpdateOCLC)) {
        echo '<strong>Updated OCLC</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

# Update ISSN
if (strlen($ISSN) != 0)
{
    if (mysqli_query($conn, $UpdateISSN)) {
        echo '<strong>Updated ISSN</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

# Update UPC
if (strlen($UPC) != 0)
{
    if (mysqli_query($conn, $UpdateUPC)) {
        echo '<strong>Updated UPC</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

# Update DOI
if (strlen($DOI) != 0)
{
    if (mysqli_query($conn, $UpdateDOI)) {
        echo '<strong>Updated DOI</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

?>