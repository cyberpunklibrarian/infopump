<?php
include 'creds.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Add New Record - Part One - Basic Information</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <style>
      html, body {
      min-height: 100%;
      }
      body, div, form, input, select, p { 
      padding: 0;
      margin: 0;
      outline: none;
      font-family: Roboto, Arial, sans-serif;
      font-size: 16px;
      color: #eee;
      }
      body {
      background: url("images/newitembackground.jpg") no-repeat center;
      background-size: cover;
      }
      h1, h2 {
      text-transform: uppercase;
      font-weight: 400;
      }
      h2 {
      margin: 0 0 0 8px;
      }
      .main-block {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      height: 100%;
      padding: 25px;
      background: rgba(0, 0, 0, 0.5); 
      }
      .left-part, form {
      padding: 25px;
      }
      .left-part {
      text-align: center;
      }
      .fa-graduation-cap {
      font-size: 72px;
      }
      form {
      background: rgba(0, 0, 0, 0.7); 
      }
      .title {
      display: flex;
      align-items: center;
      margin-bottom: 20px;
      }
      .info {
      display: flex;
      flex-direction: column;
      }
      input, select {
      padding: 5px;
      margin-bottom: 30px;
      background: transparent;
      border: none;
      border-bottom: 1px solid #eee;
      }
      input::placeholder {
      color: #eee;
      }
      option:focus {
      border: none;
      }
      option {
      background: black; 
      border: none;
      }
      .checkbox input {
      margin: 0 10px 0 0;
      vertical-align: middle;
      }
      .checkbox a {
      color: #26a9e0;
      }
      .checkbox a:hover {
      color: #85d6de;
      }
      .btn-item, button {
      padding: 10px 5px;
      margin-top: 20px;
      border-radius: 5px; 
      border: none;
      background: #26a9e0; 
      text-decoration: none;
      font-size: 15px;
      font-weight: 400;
      color: #fff;
      }
      .btn-item {
      display: inline-block;
      margin: 20px 5px 0;
      }
      button {
      width: 100%;
      }
      button:hover, .btn-item:hover {
      background: #85d6de;
      }
      @media (min-width: 568px) {
      html, body {
      height: 100%;
      }
      .main-block {
      flex-direction: row;
      height: calc(100% - 50px);
      }
      .left-part, form {
      flex: 1;
      height: auto;
      }
      }
    </style>
  </head>
  <body>
    <div class="main-block">
      <form action="addbasic.php" id="addnewrecord-basic" method="post" enctype="multipart/form-data">
        <div class="title">
          <i class="fas fa-pencil-alt"></i> 
          <h2>Add New Record - Part One - Basic Information</h2>
        </div>
        <div class="info">
          <input type="text" name="title" placeholder="Item Title - Required">
          <input type="text" name="creator" placeholder="Creator, author, director: Last Name, First Name">
          <input type="text" name="publisher" placeholder="Publisher">
          <input type="text" name="date" placeholder="Publication year">
          <select name = "seriesname">
          <option value = "" selected>Select series if applicable - Select "Singular Work" if not in a series</option>

          <?php
            $series = 'SELECT SeriesID, Description, SeriesSlug FROM SeriesIndex ORDER BY Description';

            $GetSeries = mysqli_query($conn,$series);

            while($row = mysqli_fetch_array($GetSeries)) {
              $row_SeriesID = $row['SeriesID'];
              $row_Description = $row['Description'];
              $row_SeriesSlug = $row['SeriesSlug'];
              
              echo '<option value="'.$row_SeriesSlug.'">'.$row_Description.'</option>';
            }
          ?>
          </select>

          <input type="text" name="seriesposition" placeholder="Series position - Leave blank if not part of a series">
          <select name = "type">
          <option value = "" selected>Select item type</option>

          <?php
            $types = 'SELECT TypeID, Description FROM TypeIndex ORDER BY Description';

            $GetTypes = mysqli_query($conn,$types);

            while($row = mysqli_fetch_array($GetTypes)) {
              $row_TypeID = $row['TypeID'];
              $row_Description = $row['Description'];

              echo '<option value="'.$row_TypeID.'">'.$row_Description.'</option>';
            }

          ?>
          </select>
          <input type="text" name="format" placeholder="Enter format if applicable">

          <select name = "language">
          <option value = "" selected>Select primary language</option>
          
          <?php
            $languages = 'SELECT LanguageID, Description FROM LanguageIndex ORDER BY Description';

            $GetLanguages = mysqli_query($conn,$languages);

            while($row = mysqli_fetch_array($GetLanguages)) {
              $row_LanguageID = $row['LanguageID'];
              $row_Description = $row['Description'];

              echo '<option value="'.$row_LanguageID.'">'.$row_Description.'</option>';
            }
          
          ?>

          </select>
          

        </div>
        <button type="submit" href="/">Submit and continue</button>
      </form>
    </div>
  </body>
</html>