# To Do

A list of upcoming things that need to be done. While this isn't a complete list, it's the stuff that occurred to me that should be done in the foreseeable future. This list is presented in no particular order of importance or priority.

### Incomplete

- [ ] Add search feature
- [ ] Add CSV import
- [ ] Linkable Subject Headings - to find similar items
- [ ] Linkable Creator field - to find similar items
- [ ] Export the Dublin Core record as an XML file
- [ ] Exort the Dublin Core record as a JSON file
- [ ] Linkable Series heading - to find other items in the series
- [ ] Landing page of some kind
- [ ] Error reporting feature
- [ ] Item request feature

### Pending

- [ ] Add form for creating item records within the system

### Complete

- [x] Add functionality to call the MARC record from the database
- [x] Add functionality to call the cover image via a database query
- [x] Add functionality to populate the Further Info under the Summary
- [x] Add functionality to call subjects from the database and populate Metadata
- [x] Add functionality to call languages from the database and populate Metdata
- [x] Add functionality to build the Dublin Core record from database queries
- [x] Add slug field for series
- [x] Add database table for DC:Type
- [x] Add database table for DC:Format - **No need, won't do.**