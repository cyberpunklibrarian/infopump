<?php
include 'creds.php';

# Prepare variables
$title = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
$creator = filter_var($_POST['creator'], FILTER_SANITIZE_STRING);
$creatorsplit = explode(",", $creator);
$browsecreator = $creatorsplit[1].' '.$creatorsplit[0];
$publisher = filter_var($_POST['publisher'], FILTER_SANITIZE_STRING);
$date = filter_var($_POST['date'], FILTER_SANITIZE_STRING);
$series = filter_var($_POST['seriesname'], FILTER_SANITIZE_STRING);
if (!filter_var($_POST['seriesposition'], FILTER_VALIDATE_INT) === false) {
    $seriesposition = $_POST['seriesposition'];
} elseif ($_POST['seriesposition'] == "") {
    $seriesposition = 0;
} else {
    echo("Please enter a whole number for the series position.");
}
$type = filter_var($_POST['type'], FILTER_SANITIZE_STRING);
$format = filter_var($_POST['format'], FILTER_SANITIZE_STRING);
$language = filter_var($_POST['language'], FILTER_SANITIZE_STRING);
$creationdate = date('Y-m-d H:i:s');
$modificationdate = date('Y-m-d H:i:s');

# Debugging output
# echo 'Title: '.$title.'<br />';
# echo 'Creator: '.$creator.'<br />';
# echo 'Browse Creator: '.$browsecreator.'<br />';
# echo 'Publisher: '.$publisher.'<br />';
# echo 'Date: '.$date.'<br />';
# echo 'SeriesSlug: '.$series.'<br />';
# echo 'Series Position: '.$seriesposition.'<br />';
# echo 'TypeID: '.$type.'<br />';
# echo 'Format: '.$format.'<br />';
# echo 'LanguageID: '.$language.'<br />';
# echo 'Creation Date: '.$creationdate.'<br />';
# echo 'Modification Date: '.$modificationdate.'<br />';

# Prepare query to add item record
$AddItemRecord = "INSERT INTO ItemRecords
    (Title, CreatorSort, CreatorBrowse, Publisher, Date, Type, Format, CreationDate, ModifiedDate)
    VALUES
    ('$title', '$creator', '$browsecreator', '$publisher', '$date', '$type', '$format', '$creationdate', '$modificationdate')";

# Prepare query to get the ItemRecordID right after adding it
$GetItemRecordID = "SELECT ItemRecordID
    FROM ItemRecords
    WHERE Title = '$title'
    AND CreatorSort = '$creator'
    AND CreatorBrowse = '$browsecreator'
    AND Publisher = '$publisher'
    AND Date = '$date'
    AND Type = '$type'
    AND Format = '$format'";

echo '<hr>';

echo 'Add Item SQL: '.$AddItemRecord.'<br /><br />';
echo 'Get ItemRecordID SQL: '.$GetItemRecordID.'<br /><br />';

if (mysqli_query($conn, $AddItemRecord)) {
    echo '<strong>Item added to database</strong><br />';
} else {
    echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
}

$SetItemRecordID = mysqli_query($conn,$GetItemRecordID);

while($row = mysqli_fetch_array($SetItemRecordID)) {
    $row_ItemRecordID = $row['ItemRecordID'];
    echo 'ItemRecordID: '.$row_ItemRecordID.'<br />';
}

$GetSeriesID = "SELECT SeriesID
    FROM SeriesIndex
    WHERE SeriesSlug = '$series'";

$SetSeriesID = mysqli_query($conn,$GetSeriesID);

while($row = mysqli_fetch_array($SetSeriesID)) {
    $row_SeriesID = $row['SeriesID'];
    echo 'Series ID: '.$row_SeriesID.'<br />';
}

$ItemSeries = "INSERT INTO ItemSeries
    (ItemRecordID, SeriesID, Description)
    VALUES
    ('$row_ItemRecordID', '$row_SeriesID', '$seriesposition')";

$ItemLanguage = "INSERT INTO ItemLanguages
    (ItemRecordID, LanguageID)
    VALUES
    ('$row_ItemRecordID', '$language')";

if (mysqli_query($conn, $ItemSeries)) {
    echo '<strong>Item series updated</strong><br />';
} else {
    echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
}

if (mysqli_query($conn, $ItemLanguage)) {
    echo '<strong>Item language updated</strong><br />';
} else {
    echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
}

header("Location: newrecord02.php?itemID=$row_ItemRecordID");

?>

