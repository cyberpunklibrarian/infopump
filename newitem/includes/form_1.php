<?php	
	if (empty($_POST['primarytitle']) && strlen($_POST['primarytitle']) == 0 || empty($_POST['secondarytitle']) && strlen($_POST['secondarytitle']) == 0 || empty($_POST['publisher']) && strlen($_POST['publisher']) == 0 || empty($_POST['pubyear']) && strlen($_POST['pubyear']) == 0)
	{
		return false;
	}
	
	$primarytitle = $_POST['primarytitle'];
	$secondarytitle = $_POST['secondarytitle'];
	$publisher = $_POST['publisher'];
	$pubyear = $_POST['pubyear'];
	$seriesselect = $_POST['seriesselect'];
	$seriesposition = $_POST['seriesposition'];
	$itemtype = $_POST['itemtype'];
	$itemformat = $_POST['itemformat'];
	$langaugeselect = $_POST['langaugeselect'];
	$isbn10 = $_POST['isbn10'];
	$isbn13 = $_POST['isbn13'];
	$oclc = $_POST['oclc'];
	$issn = $_POST['issn'];
	$upc = $_POST['upc'];
	$doi = $_POST['doi'];
	$subjectheading1 = $_POST['subjectheading1'];
	$subjectheading2 = $_POST['subjectheading2'];
	$subjectheading3 = $_POST['subjectheading3'];
	$subjectheading4 = $_POST['subjectheading4'];
	$subjectheading5 = $_POST['subjectheading5'];
	$summary = $_POST['summary'];
	$linkabe = $_POST['linkabe'];
	$linkauthor = $_POST['linkauthor'];
	$linkff = $_POST['linkff'];
	$linkgoodreads = $_POST['linkgoodreads'];
	$linklt = $_POST['linklt'];
	$linkworldcat = $_POST['linkworldcat'];
	
	$to = 'receiver@yoursite.com'; // Email submissions are sent to this email

	// Create email	
	$email_subject = "Message from a Blocs website.";
	$email_body = "You have received a new message. \n\n".
				  "Primarytitle: $primarytitle \nSecondarytitle: $secondarytitle \nPublisher: $publisher \nPubyear: $pubyear \nSeriesselect: $seriesselect \nSeriesposition: $seriesposition \nItemtype: $itemtype \nItemformat: $itemformat \nLangaugeselect: $langaugeselect \nIsbn10: $isbn10 \nIsbn13: $isbn13 \nOclc: $oclc \nIssn: $issn \nUpc: $upc \nDoi: $doi \nSubjectheading1: $subjectheading1 \nSubjectheading2: $subjectheading2 \nSubjectheading3: $subjectheading3 \nSubjectheading4: $subjectheading4 \nSubjectheading5: $subjectheading5 \nSummary: $summary \nLinkabe: $linkabe \nLinkauthor: $linkauthor \nLinkff: $linkff \nLinkgoodreads: $linkgoodreads \nLinklt: $linklt \nLinkworldcat: $linkworldcat \n";
	$headers = "MIME-Version: 1.0\r\nContent-type: text/plain; charset=UTF-8\r\n";	
	$headers .= "From: contact@yoursite.com\n";
	$headers .= "Reply-To: DoNotReply@yoursite.com";	
	
	mail($to,$email_subject,$email_body,$headers); // Post message
	return true;			
?>