<?php
// Credentials
include 'creds.php';

// Get and use an item record ID (ItemRecordID) to use for a data pull
$ItemID = htmlspecialchars($_GET["itemid"]);

// -------------------- DATABASE QUERIES --------------------

// --- BEGIN HERO SECTION ---

// Hero section: Set up database query for hero section
$Hero = "SELECT ir.Title AS Title,
    ir.CreatorBrowse AS Creator,
    its.Description AS ItemSeries,
    si.Description AS SeriesName 
    FROM ItemRecords AS ir 
    JOIN ItemSeries AS its ON its.ItemRecordID = ir.ItemRecordID 
    JOIN SeriesIndex AS si ON si.SeriesID = its.SeriesID 
    WHERE ir.ItemRecordID = '$ItemID'";

// Execute the query to get item record information
$HeroResult = mysqli_query($conn,$Hero);
// Assign results to variables
while($row = mysqli_fetch_array($HeroResult))
{
    $row_Title = $row['Title'];
    $row_Creator = $row['Creator'];
	$row_ItemSeries = $row['ItemSeries'];
	$row_SeriesName = $row['SeriesName'];
}

// Hero section: Set up database query to get cover image
$CoverImage = "SELECT ici.ItemCoverID,
    ici.FileName,
    ici.AltText
    FROM ItemCoverIndex AS ici
    JOIN ItemCovers AS ic ON ic.ItemCoverID = ici.ItemCoverID
    WHERE ic.ItemRecordID = '$ItemID'";
// Execute the query to get the cover image
$CoverImageResult = mysqli_query($conn,$CoverImage);

// Assign results to variables
while($row = mysqli_fetch_array($CoverImageResult))
{
    $row_ItemCoverID = $row['ItemCoverID'];
    $row_FileName = $row['FileName'];
    $row_AltText = $row['AltText'];
}

// --- END HERO SECTION ---

// ---- BEGIN FURTHER INFORMATION ----
// Set up the query to get further information from database
$FurtherInfo = "SELECT fii.Description AS FIDescription,
    ifi.Content AS FIContent
    FROM ItemFurtherInfo AS ifi
    JOIN FurtherInfoIndex AS fii on fii.FurtherInfoID = ifi.FurtherInfoID
    WHERE ifi.ItemRecordID = '$ItemID'
    ORDER BY fii.Description";
// --- END FURTHER INFORMATION ---


// ---- BEGIN SUMMARY DATABASE JOBS ----
// Set up the database query to pull the summary
$Summary = "SELECT Summary
    FROM Summaries
    WHERE ItemRecordID = '$ItemID'";
// Exectue the query to get summary information
$SummaryResult = mysqli_query($conn,$Summary);
// Assign to variables
while($row = mysqli_fetch_array($SummaryResult))
{
    $row_Summary = $row['Summary'];
}
// ----- END SUMMARY DATABASE JOBS -----

// --- BEGIN METADATA SECTION ---

// Metadata: Set up the query for general metadata
$Meta = "SELECT ItemRecordID AS MetaItemRecordID,
    Title AS MetaTitle,
    CreatorSort AS MetaCreatorSort,
    Publisher AS MetaPublisher,
    Date AS MetaDate,
    Type AS MetaType,
    Format AS MetaFormat,
    ISBN10 AS MetaISBN10,
    ISBN13 AS MetaISBN13,
    CreationDate AS MetaCreationDate,
    ModifiedDate AS MetaModifiedDate
    FROM ItemRecords
    WHERE ItemRecordID = '$ItemID'";
// Execute the query to get general metadata
$MetaResult = mysqli_query($conn,$Meta);

// Assign results to variables
while($row = mysqli_fetch_array($MetaResult))
	{
	$row_MetaItemRecordID = $row['MetaItemRecordID'];
	$row_MetaTitle = $row['MetaTitle'];
	$row_MetaCreatorSort = $row['MetaCreatorSort'];
	$row_MetaPublisher = $row['MetaPublisher'];
	$row_MetaDate = $row['MetaDate'];
	$row_MetaType = $row['MetaType'];
	$row_MetaFormat = $row['MetaFormat'];
	$row_MetaISBN10 = $row['MetaISBN10'];
	$row_MetaISBN13 = $row['MetaISBN13'];
	$row_MetaCreationDate = $row['MetaCreationDate'];
	$row_MetaModifiedDate = $row['MetaModifiedDate'];
    }

// Metadata: Get subject headings

// Set up the query for subject headings
$Subjects = "SELECT shi.Description AS SubjectDescription
    FROM SubjectHeadingIndex AS shi
    INNER JOIN ItemSubjects AS its ON its.SubjectID = shi.SubjectID
    WHERE its.ItemRecordID = '$ItemID'";
// Execute the query to get subject headings
$SubjectsResult = mysqli_query($conn,$Subjects);

// Metadata: Get language information

// Set up the query for languages
$Language = "SELECT li.Description AS Language,
    li.ISOCode AS ISOCode
    FROM ItemLanguages AS il
    JOIN LanguageIndex AS li ON li.LanguageID = il.LanguageID
    WHERE il.ItemRecordID = '$ItemID'";
// Execute the query to get language information
$LanguageResult = mysqli_query($conn,$Language);

// END METADATA SECTION

// --- BEGIN DUBLIN CORE SECTION ---

$DCSubjects = "SELECT shi.Description AS SubjectDescription
    FROM SubjectHeadingIndex AS shi
    INNER JOIN ItemSubjects AS its ON its.SubjectID = shi.SubjectID
    WHERE its.ItemRecordID = '$ItemID'";
// Execute the query to get subject headings
$DCSubjectsResult = mysqli_query($conn,$DCSubjects);

// Set up the query for languages in the Dublin Core record
$DCLanguage = "SELECT li.Description AS Language,
    li.ISOCode AS ISOCode
    FROM ItemLanguages AS il
    JOIN LanguageIndex AS li ON li.LanguageID = il.LanguageID
    WHERE il.ItemRecordID = '$ItemID'";
// Execute the query to get language information
$DCLanguageResult = mysqli_query($conn,$Language);

// ----- END DATABASE JOBS -----

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php echo '<title>Item Record: '.$row_Title.'</title>'; ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic|Roboto:400,300,700' rel='stylesheet' type='text/css'>
	<!-- Animate -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon -->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/style.css">


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<div id="fh5co-offcanvas">
		<a href="#" class="fh5co-close-offcanvas js-fh5co-close-offcanvas"><span><i class="icon-cross3"></i> <span>Close</span></span></a>
		<div class="fh5co-bio">
			<figure>
				<img src="images/person1.jpg" alt="Free HTML5 Bootstrap Template" class="img-responsive">
			</figure>
			<h3 class="heading">About Me</h3>
			<h2>Emily Tran Le</h2>
			<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
			<ul class="fh5co-social">
				<li><a href="#"><i class="icon-twitter"></i></a></li>
				<li><a href="#"><i class="icon-facebook"></i></a></li>
				<li><a href="#"><i class="icon-instagram"></i></a></li>
			</ul>
		</div>

		<div class="fh5co-menu">
			<div class="fh5co-box">
				<h3 class="heading">Categories</h3>
				<ul>
					<li><a href="#">Travel</a></li>
					<li><a href="#">Style</a></li>
					<li><a href="#">Photography</a></li>
					<li><a href="#">Food &amp; Drinks</a></li>
					<li><a href="#">Culture</a></li>
				</ul>
			</div>
			<div class="fh5co-box">
				<h3 class="heading">Search</h3>
				<form action="#">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Type a keyword">
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- END #fh5co-offcanvas -->
	<header id="fh5co-header">
		
		<div class="container-fluid">

			<div class="row">
				<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
				<ul class="fh5co-social">
					<li><a href="#"><i class="icon-twitter"></i></a></li>
					<li><a href="#"><i class="icon-facebook"></i></a></li>
					<li><a href="#"><i class="icon-instagram"></i></a></li>
				</ul>
				<div class="col-lg-12 col-md-12 text-center">
					<?php echo '<h1 id="fh5co-logo">Item Record '.$ItemID.'</h1>';?>
				</div>

			</div>
		
		</div>

	</header>
	<a href="#" class="fh5co-post-prev"><span><i class="icon-chevron-left"></i> Prev</span></a>
	<a href="#" class="fh5co-post-next"><span>Next <i class="icon-chevron-right"></i></span></a>
	<!-- END #fh5co-header -->
	<div class="container-fluid">
		<div class="row fh5co-post-entry single-entry">
			<article class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
				<figure class="animate-box">
					<?php echo '<img src="images/'.$row_FileName.'" alt="'.$row_AltText.'" title="'.$row_AltText.'" class="img-responsive">';?>
				</figure>

                
				<?php echo '<span class="fh5co-meta animate-box">'.$row_MetaFormat.'</span>';?>
				<?php echo '<h2 class="fh5co-article-title animate-box">'.$row_MetaTitle.'</h2>';?>
				<?php echo '<span class="fh5co-meta fh5co-date animate-box">'.$row_Creator.'</span>';?>
				
				<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-left content-article">
					<div class="row">
						<div class="col-lg-7 cp-r animate-box">
                            <?php echo $row_Summary;
                            echo '<br />';
                            echo '<h3>Further Information</h3>';
                            //Execute the query to get further information data
                        	$FurtherInfoResult = mysqli_query($conn,$FurtherInfo);
                        	// Assign further information data to variables
                        	while($row = mysqli_fetch_array($FurtherInfoResult))
                        	{
                            	$row_FIDescription = $row['FIDescription'];
                            	$row_FIContent = $row['FIContent'];

                            	echo '<a href="'.$row_FIContent.'">'.$row_FIDescription.'</a><br />';
                        	}
                            ?>
						</div>
						<div class="col-lg-5 animate-box">
							<div class="fh5co-highlight right">
								<h4>Metadata</h4>
								<p>
                                    <?php
                                    echo '<strong>Title:</strong> '.$row_MetaTitle.'<br />';
                                    echo '<strong>Creator:</strong> '.$row_MetaCreatorSort.'<br />';
                                    while($row = mysqli_fetch_array($SubjectsResult))
								    {
		    							$row_SubjectDescription = $row['SubjectDescription'];
	    								echo '<strong>Subject:</strong> '.$row_SubjectDescription.'<br />';
    								}
                                    echo '<strong>Publisher:</strong> '.$row_MetaPublisher.'<br />';
                                    echo '<strong>Date:</strong> '.$row_MetaDate.'<br />';
                                    echo '<strong>Type:</strong> '.$row_MetaType.'<br />';
                                    echo '<strong>Format:</strong> '.$row_MetaFormat.'<br />';
                                    echo '<strong>Identifier:</strong> '.$row_MetaISBN10.'<br />';
                                    echo '<strong>Identifier:</strong> '.$row_MetaISBN13.'<br />';
                                    while($row = mysqli_fetch_array($LanguageResult))
							        {
								        $row_Language = $row['Language'];
								        echo '<strong>Language:</strong> '.$row_Language.'<br />';
							        }
                                    echo '<strong>Item Control Number:</strong> '.$row_MetaItemRecordID.'<br />';
                                    echo '<strong>Created:</strong> '.$row_MetaCreationDate.'<br />';
                                    echo '<strong>Modified:</strong> '.$row_MetaModifiedDate;
                                    ?>
                                </p>
							</div>
						</div>
					</div>

					<!--<div class="row rp-b">
						<div class="col-md-12 animate-box">
							<blockquote>
								<p>&ldquo;She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove &rdquo; <cite>&mdash; Jean Smith</cite></p>
							</blockquote>
						</div>
					</div> -->

					<div class="row rp-b">
                        <hr>
                        <div class="col-md-12 animate-box">
                            <h3>Dublin Core Catalogue Record</h3>

                            <?php
							echo '<code>';
							
                            echo '&lt;meta name="DC.Title" content="'.$row_MetaTitle.'"&gt;<br />';
                            echo '&lt;meta name="DC.Creator" content="'.$row_MetaCreatorSort.'"&gt;<br />';
                            while($row = mysqli_fetch_array($DCSubjectsResult))
							{
							    $row_SubjectDescription = $row['SubjectDescription'];
								echo '&lt;meta name="DC.Subject" content="'.$row_SubjectDescription.'"&gt;<br />';
							}
                            
							$stripSummary = str_replace("<br />", " ", $row_Summary);
							$dcSummary = str_replace(array("\r", "\n"), '', $stripSummary);
							echo '&lt;meta name="DC.Description" content="'.$dcSummary.'"&gt;<br />';
                            echo '&lt;meta name="DC.Publisher" content="'.$row_MetaPublisher.'"&gt;<br />';
                            echo '&lt;meta name="DC.Date" content="'.$row_MetaDate.'"&gt;<br />';
                            echo '&lt;meta name="DC.Type" content="'.$row_MetaType.'"&gt;<br />';
                            echo '&lt;meta name="DC.Format" content="'.$row_MetaFormat.'"&gt;<br />';
                            echo '&lt;meta name="DC.Identifier" content="ISBN 10: '.$row_MetaISBN10.'"&gt;<br />';
                            echo '&lt;meta name="DC.Identifier" content="ISBN 13: '.$row_MetaISBN13.'"&gt;<br />';
							while($row = mysqli_fetch_array($DCLanguageResult))
							{
								$row_Language = $row['Language'];
								$row_ISOCode = $row['ISOCode'];
								echo '&lt;meta name="DC.Language" content="'.$row_Language.'"&gt;<br />';
								echo '&lt;meta name="DC.Language" content="'.$row_ISOCode.'"&gt;<br />';
							}
                        	echo '</code>';
							?>
						</div>
                    </div> 
					
					
				</div>
			</article>
		</div>
	</div>

	<footer id="fh5co-footer">
		<p><small>&copy; Creative Commons By-NC-SA<br> Design by <a href="http://freehtml5.co" target="_blank">FREEHTML5.co</a></small></p>
	</footer>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Main JS -->
	<script src="js/main.js"></script>

	</body>
</html>

