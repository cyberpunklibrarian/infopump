<?php
include 'creds.php';
?>
<!DOCTYPE HTML>
<!--
	Story by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Prototype: Book Page - Neuromancer</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper" class="divided">

            <!-- Hero PHP -->
                <section class="banner style1 orient-left content-align-left image-position-right fullscreen onload-image-fade-in onload-content-fade-right">
					<div class="content">
                    <?php

					$heroresult = mysqli_query($conn,"SELECT ir.Title AS Title ,ir.CreatorBrowse AS Creator,its.Description AS ItemSeries,si.Description AS SeriesName FROM ItemRecords AS ir JOIN ItemSeries AS its ON its.ItemRecordID = ir.ItemRecordID JOIN SeriesIndex AS si ON si.SeriesID = its.SeriesID WHERE ir.ItemRecordID = 1");

                    while($row = mysqli_fetch_array($heroresult))
                    {
                        $row_Title = $row['Title'];
                        $row_Creator = $row['Creator'];
						$row_ItemSeries = $row['ItemSeries'];
						$row_SeriesName = $row['SeriesName'];

						echo '<h1>'.$row_Title.'</h1>';
						
						//echo '<p>Book 1 of the Sprawl Trilogy</p>';
						echo '<p>'.$row_SeriesName.': '.$row_ItemSeries.'</p>';
						echo '<p class="major">'.$row_Creator.'</p>';
					}
                    
					echo '</div>';
					echo '<div class="image">';
						echo '<img src="images/neuromancer.webp" alt="" />';
					echo '</div>';
				echo '</section>';
                    
                ?>

				<!-- Summary -->
					<section class="spotlight style1 orient-right content-align-left image-position-center onscroll-image-fade-in" id="first">
						<div class="content">
						<?php
						$summaryresult = mysqli_query($conn,"SELECT Summary FROM Summaries WHERE ItemRecordID = 1");
						$furtherinforesult = mysqli_query($conn,"SELECT fii.Description AS Description, ifi.Content AS Content FROM ItemFurtherInfo AS ifi JOIN FurtherInfoIndex AS fii on fii.FurtherInfoID = ifi.FurtherInfoID WHERE ifi.ItemRecordID = 1 ORDER BY fii.Description");

						while($row = mysqli_fetch_array($summaryresult))
						{
							$row_Summary = $row['Summary'];
						}
						echo '<h3>Summary</h3>';
						echo $row_Summary;
						?>

							<h3>Further Information</h3>
							<?php
							echo '<p>';
							while($row = mysqli_fetch_array($furtherinforesult))
								{
									$row_Description = $row['Description'];
									$row_Content = $row['Content'];
									echo '<a href="'.$row_Content.'">'.$row_Description.'</a><br />';
								}
								
							echo '</p>';
							?>
						</div>
						<div class="image">
							<img src="images/summary.jpg" alt="" />
						</div>
					</section>

				<!-- Metadata -->
					<section class="spotlight style1 orient-left content-align-left image-position-center onscroll-image-fade-in">
						<div class="content">
							<h2>Metadata</h2>
							<?php 

						$metaresult = mysqli_query($conn,"SELECT ItemRecordID,Title,CreatorSort,Publisher,Date,Type,Format,ISBN10,ISBN13,CreationDate,ModifiedDate FROM ItemRecords WHERE ItemRecordID = 1");
						$subjectresult = mysqli_query($conn,"SELECT shi.Description AS Description FROM SubjectHeadingIndex AS shi INNER JOIN ItemSubjects AS its ON its.SubjectID = shi.SubjectID WHERE its.ItemRecordID = 1");
						$languageresult = mysqli_query($conn,"SELECT li.Description AS Language	FROM ItemLanguages AS il JOIN LanguageIndex AS li ON li.LanguageID = il.LanguageID WHERE il.ItemRecordID = 1 ");

						while($row = mysqli_fetch_array($metaresult))
						{
							$row_ItemRecordID = $row['ItemRecordID'];
							$row_Title = $row['Title'];
							$row_CreatorSort = $row['CreatorSort'];
							$row_Publisher = $row['Publisher'];
							$row_Date = $row['Date'];
							$row_Type = $row['Type'];
							$row_Format = $row['Format'];
							$row_ISBN10 = $row['ISBN10'];
							$row_ISBN13 = $row['ISBN13'];
							$row_CreationDate = $row['CreationDate'];
							$row_ModifiedDate = $row['ModifiedDate'];
						

							echo '<p><strong>Title:</strong> '.$row_Title.'<br />';
							echo '<strong>Creator:</strong> '.$row_CreatorSort.'<br />';
							while($row = mysqli_fetch_array($subjectresult))
								{
									$row_Description = $row['Description'];
									echo '<strong>Subject:</strong> '.$row_Description.'<br />';
								}	
							echo '<strong>Publisher:</strong> '.$row_Publisher.'<br />';
							echo '<strong>Date:</strong> '.$row_Date.'<br />';
							echo '<strong>Type:</strong> '.$row_Type.'<br />';
							echo '<strong>Format:</strong> '.$row_Format.'<br />';
							echo '<strong>Identifier:</strong> ISBN 10: '.$row_ISBN10.'<br />';
							echo '<strong>Identifier:</strong> ISBN 13: '.$row_ISBN13.'<br />';
							while($row = mysqli_fetch_array($languageresult))
								{
									$row_Language = $row['Language'];
									echo '<strong>Language:</strong> '.$row_Language.'<br />';
								}
							echo '<strong>Item Control Number:</strong> '.$row_ItemRecordID.'<br />';
							echo '<strong>Creation Date:</strong> '.$row_CreationDate.'<br />';
							echo '<strong>Last Modified:</strong> '.$row_ModifiedDate;
							echo '</p>';
						}
						?>
						</div>
						<div class="image">
							<img src="images/metadata.jpg" alt="" />
						</div>
					</section>

				<!-- Dublin Core -->
					<section class="spotlight style1 orient-right content-align-left image-position-center onscroll-image-fade-in">
						<div class="content">
							<h2>Dublin Core Record</h2>
							<?php
							echo '<pre><code>';
							$metaresult = mysqli_query($conn,"SELECT ItemRecordID,Title,CreatorSort,Publisher,Date,Type,Format,ISBN10,ISBN13,CreationDate,ModifiedDate FROM ItemRecords WHERE ItemRecordID = 1");
							$subjectresult = mysqli_query($conn,"SELECT shi.Description AS Description FROM SubjectHeadingIndex AS shi INNER JOIN ItemSubjects AS its ON its.SubjectID = shi.SubjectID WHERE its.ItemRecordID = 1");
							$summaryresult = mysqli_query($conn,"SELECT Summary FROM Summaries WHERE ItemRecordID = 1");
							$languageresult = mysqli_query($conn,"SELECT li.Description AS Language, li.ISOCode AS ISOCode FROM ItemLanguages AS il JOIN LanguageIndex AS li ON li.LanguageID = il.LanguageID WHERE il.ItemRecordID = 1 ");
							while($row = mysqli_fetch_array($metaresult))
						{
							$row_ItemRecordID = $row['ItemRecordID'];
							$row_Title = $row['Title'];
							$row_CreatorSort = $row['CreatorSort'];
							$row_Publisher = $row['Publisher'];
							$row_Date = $row['Date'];
							$row_Type = $row['Type'];
							$row_Format = $row['Format'];
							$row_ISBN10 = $row['ISBN10'];
							$row_ISBN13 = $row['ISBN13'];
							$row_CreationDate = $row['CreationDate'];
							$row_ModifiedDate = $row['ModifiedDate'];
echo '&lt;meta name="DC.Title" content="'.$row_Title.'"&gt;<br />';
echo '&lt;meta name="DC.Creator" content="'.$row_CreatorSort.'"&gt;<br />';
							while($row = mysqli_fetch_array($subjectresult))
							{
								$row_Description = $row['Description'];
								echo '&lt;meta name="DC.Subject" content="'.$row_Description.'"&gt;<br />';
							}
							while($row = mysqli_fetch_array($summaryresult))
							{
								$row_Summary = $row['Summary'];
								$stripSummary = strip_tags($row_Summary);
								$dcSummary = str_replace(array("\r", "\n"), '', $stripSummary);
								echo '&lt;meta name="DC.Description" content="'.$dcSummary.'"&gt;<br />';
							}
echo '&lt;meta name="DC.Publisher" content="'.$row_Publisher.'"&gt;<br />';
echo '&lt;meta name="DC.Date" content="'.$row_Date.'"&gt;<br />';
echo '&lt;meta name="DC.Type" content="'.$row_Type.'"&gt;<br />';
echo '&lt;meta name="DC.Format" content="'.$row_Format.'"&gt;<br />';
echo '&lt;meta name="DC.Identifier" content="ISBN 10: '.$row_ISBN10.'"&gt;<br />';
echo '&lt;meta name="DC.Identifier" content="ISBN 13: '.$row_ISBN13.'"&gt;<br />';
							while($row = mysqli_fetch_array($languageresult))
								{
									$row_Language = $row['Language'];
									$row_ISOCode = $row['ISOCode'];
									echo '&lt;meta name="DC.Language" content="'.$row_Language.'"&gt;<br />';
									echo '&lt;meta name="DC.Language" content="'.$row_ISOCode.'"&gt;<br />';
								}
							echo '</code></pre>';
						}
							?>
						</div>
						<div class="image">
							<img src="images/dublincore.png" alt="" />
						</div>
					</section>
				
				<!-- Four A -->
					<section class="spotlight style1 orient-left content-align-left image-position-center onscroll-image-fade-in">
						<div class="content">
							<h2>Basic MARC Record</h2>
							<p>
								001	180898951<br />
								003	MePoLT<br />
								005	20210528221501.0<br />
								020    	$a0441569595 <br />
								040    	$aMePoLT $cMePoLT $erda <br />
								090   4	$aPS3557 .I2264<br /> 
								092   4	$a813.54 <br />
								100 1  	$aGibson, William. <br />
								245 1 0	$aNeuromancer. <br />
								264   1	$aAce (1986), Edition $b 1st, 271 pages, $c1986. <br />
								300    	$a288 p. <br />
								520    	$aNeuromancer by William Gibson (1984) <br />
								923    	$aYour library 
							</p>
							
						</div>
						<div class="image">
							<img src="images/marc.jpg" alt="" />
						</div>
					</section>	

				

				<!-- Footer -->
					<footer class="wrapper style1 align-center">
						<div class="inner">
							<ul class="icons">
								<li><a href="#" class="icon brands style2 fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon brands style2 fa-facebook-f"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon brands style2 fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon brands style2 fa-linkedin-in"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon style2 fa-envelope"><span class="label">Email</span></a></li>
							</ul>
							<p>&copy; Creative Commons By-NC-SA.<br />Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
						</div>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>