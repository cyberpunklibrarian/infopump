<?php
// Credentials
include 'creds.php';

// Get and use an item record ID (ItemRecordID) to use for a data pull
$ItemID = htmlspecialchars($_GET["itemid"]);

// ----- DATABASE QUERIES -----

// ----- BEGIN HERO DATABASE JOBS -----
// Set up database query for hero section
$Hero = "SELECT ir.Title AS Title,
    ir.CreatorBrowse AS Creator,
    its.Description AS ItemSeries,
    si.Description AS SeriesName 
    FROM ItemRecords AS ir 
    JOIN ItemSeries AS its ON its.ItemRecordID = ir.ItemRecordID 
    JOIN SeriesIndex AS si ON si.SeriesID = its.SeriesID 
    WHERE ir.ItemRecordID = '$ItemID'";

// Execute the query to get item record information
$HeroResult = mysqli_query($conn,$Hero);
// Assign results to variables
while($row = mysqli_fetch_array($HeroResult))
{
    $row_Title = $row['Title'];
    $row_Creator = $row['Creator'];
	$row_ItemSeries = $row['ItemSeries'];
	$row_SeriesName = $row['SeriesName'];
}

// Set up database query to get cover image

$CoverImage = "SELECT ici.ItemCoverID,
    ici.FileName,
    ici.AltText
    FROM ItemCoverIndex AS ici
    JOIN ItemCovers AS ic ON ic.ItemCoverID = ici.ItemCoverID
    WHERE ic.ItemRecordID = '$ItemID'";
// Execute the query to get the cover image.
$CoverImageResult = mysqli_query($conn,$CoverImage);

while($row = mysqli_fetch_array($CoverImageResult))
{
    $row_ItemCoverID = $row['ItemCoverID'];
    $row_FileName = $row['FileName'];
    $row_AltText = $row['AltText'];
}

// ----- END HERO DATABASE JOBS -----

// ----- BEGIN SUMMARY DATABASE JOBS -----
// Set up the database query to pull the summary
$Summary = "SELECT Summary
    FROM Summaries
    WHERE ItemRecordID = '$ItemID'";
// Exectue the query to get summary information
$SummaryResult = mysqli_query($conn,$Summary);
// Assign to variables
while($row = mysqli_fetch_array($SummaryResult))
{
    $row_Summary = $row['Summary'];
}
// ----- END SUMMARY DATABASE JOBS -----

// ----- BEGIN FURTHER INFORMATION DATABASE JOBS -----
// Set up the query to get further information from database
$FurtherInfo = "SELECT fii.Description AS FIDescription,
    ifi.Content AS FIContent
    FROM ItemFurtherInfo AS ifi
    JOIN FurtherInfoIndex AS fii on fii.FurtherInfoID = ifi.FurtherInfoID
    WHERE ifi.ItemRecordID = '$ItemID'
    ORDER BY fii.Description";
// ----- END FURTHER INFORMATION DATABASE JOBS -----

// ----- BEGIN METADATA DATABASE JOBS -----

// Set up the query for general metadata
$Meta = "SELECT ItemRecordID AS MetaItemRecordID,
    Title AS MetaTitle,
    CreatorSort AS MetaCreatorSort,
    Publisher AS MetaPublisher,
    Date AS MetaDate,
    Type AS MetaType,
    Format AS MetaFormat,
    ISBN10 AS MetaISBN10,
    ISBN13 AS MetaISBN13,
    CreationDate AS MetaCreationDate,
    ModifiedDate AS MetaModifiedDate
    FROM ItemRecords
    WHERE ItemRecordID = '$ItemID'";
// Execute the query to get general metadata
$MetaResult = mysqli_query($conn,$Meta);

// Set up the query for subject headings
$Subjects = "SELECT shi.Description AS SubjectDescription
    FROM SubjectHeadingIndex AS shi
    INNER JOIN ItemSubjects AS its ON its.SubjectID = shi.SubjectID
    WHERE its.ItemRecordID = '$ItemID'";
// Execute the query to get subject headings
$SubjectsResult = mysqli_query($conn,$Subjects);

// Set up the query for languages
$Language = "SELECT li.Description AS Language,
    li.ISOCode AS ISOCode
    FROM ItemLanguages AS il
    JOIN LanguageIndex AS li ON li.LanguageID = il.LanguageID
    WHERE il.ItemRecordID = '$ItemID'";
// Execute the query to get language information
$LanguageResult = mysqli_query($conn,$Language);

$DCSubjects = "SELECT shi.Description AS SubjectDescription
    FROM SubjectHeadingIndex AS shi
    INNER JOIN ItemSubjects AS its ON its.SubjectID = shi.SubjectID
    WHERE its.ItemRecordID = '$ItemID'";
// Execute the query to get subject headings
$DCSubjectsResult = mysqli_query($conn,$DCSubjects);

// Set up the query for languages in the Dublin Core record
$DCLanguage = "SELECT li.Description AS Language,
    li.ISOCode AS ISOCode
    FROM ItemLanguages AS il
    JOIN LanguageIndex AS li ON li.LanguageID = il.LanguageID
    WHERE il.ItemRecordID = '$ItemID'";
// Execute the query to get language information
$DCLanguageResult = mysqli_query($conn,$Language);

// ----- END METADATA DATABASE JOBS -----

// ----- BEGIN MARC DATABASE JOBS -----
$MARC = "SELECT MARC
	FROM MARCRecords
	WHERE ItemRecordID = '$ItemID'";

$MARCResult = mysqli_query($conn,$MARC);
// ----- END MARC DATABASE JOBS -----
?>

<!DOCTYPE HTML>
<!--
	Story by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<?php echo '<title>Item Record: '.$row_Title.'</title>'; ?>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper" class="divided">

            <!-- Hero PHP -->
                <section class="banner style1 orient-left content-align-left image-position-right fullscreen onload-image-fade-in onload-content-fade-right">
					<div class="content">
                    <?php

						echo '<h1>'.$row_Title.'</h1>';
						
						echo '<p>'.$row_SeriesName.': '.$row_ItemSeries.'</p>';
						echo '<p class="major">'.$row_Creator.'</p>';
                    
						echo '</div>';
						echo '<div class="image">';
						echo '<a href = "images/'.$row_FileName.'" target="_blank"><img src = "images/'.$row_FileName.'" alt = "'.$row_AltText.'" title = "'.$row_AltText.' - Click for full size image"></a>';
                    	echo '</div>';
						echo '</section>';
                    
                	?>

				<!-- Summary -->
					<section class="spotlight style1 orient-right content-align-left image-position-center onscroll-image-fade-in" id="first">
						<div class="content">
						<?php						

							echo '<h3>Summary</h3>';
							echo $row_Summary;
                        
							echo '<h3>Further Information</h3>';
							echo '<p>';
                        
                        	//Execute the query to get further information data
                        	$FurtherInfoResult = mysqli_query($conn,$FurtherInfo);
                        	// Assign further information data to variables
                        	while($row = mysqli_fetch_array($FurtherInfoResult))
                        	{
                            	$row_FIDescription = $row['FIDescription'];
                            	$row_FIContent = $row['FIContent'];

                            	echo '<a href="'.$row_FIContent.'">'.$row_FIDescription.'</a><br />';
                        	}

							echo '</p>';
						?>

						</div>
						<div class="image">
							<img src="images/summary.jpg" alt="" />
						</div>
					</section>

				<!-- Metadata -->
					<section class="spotlight style1 orient-left content-align-left image-position-center onscroll-image-fade-in">
						<div class="content">
							<h2>Metadata</h2>
							<?php 

							while($row = mysqli_fetch_array($MetaResult))
							{
								$row_MetaItemRecordID = $row['MetaItemRecordID'];
								$row_MetaTitle = $row['MetaTitle'];
								$row_MetaCreatorSort = $row['MetaCreatorSort'];
								$row_MetaPublisher = $row['MetaPublisher'];
								$row_MetaDate = $row['MetaDate'];
								$row_MetaType = $row['MetaType'];
								$row_MetaFormat = $row['MetaFormat'];
								$row_MetaISBN10 = $row['MetaISBN10'];
								$row_MetaISBN13 = $row['MetaISBN13'];
								$row_MetaCreationDate = $row['MetaCreationDate'];
								$row_MetaModifiedDate = $row['MetaModifiedDate'];

								echo '<p><strong>Title:</strong> '.$row_MetaTitle.'<br />';
								echo '<strong>Creator:</strong> '.$row_MetaCreatorSort.'<br />';
							while($row = mysqli_fetch_array($SubjectsResult))
								{
									$row_SubjectDescription = $row['SubjectDescription'];
									echo '<strong>Subject:</strong> '.$row_SubjectDescription.'<br />';
								}	
								echo '<strong>Publisher:</strong> '.$row_MetaPublisher.'<br />';
								echo '<strong>Date:</strong> '.$row_MetaDate.'<br />';
								echo '<strong>Type:</strong> '.$row_MetaType.'<br />';
								echo '<strong>Format:</strong> '.$row_MetaFormat.'<br />';
								echo '<strong>Identifier:</strong> ISBN 10: '.$row_MetaISBN10.'<br />';
								echo '<strong>Identifier:</strong> ISBN 13: '.$row_MetaISBN13.'<br />';
							while($row = mysqli_fetch_array($LanguageResult))
							{
								$row_Language = $row['Language'];
								echo '<strong>Language:</strong> '.$row_Language.'<br />';
							}
								echo '<strong>Item Control Number:</strong> '.$row_MetaItemRecordID.'<br />';
								echo '<strong>Creation Date:</strong> '.$row_MetaCreationDate.'<br />';
								echo '<strong>Last Modified:</strong> '.$row_MetaModifiedDate;
								echo '</p>';
							}
							?>
						</div>
						<div class="image">
							<img src="images/metadata.jpg" alt="" />
						</div>
					</section>

				<!-- Dublin Core -->
					<section class="spotlight style1 orient-right content-align-left image-position-center onscroll-image-fade-in">
						<div class="content">
							<h2>Dublin Core Record</h2>
							<?php
							echo '<pre><code>';
							
echo '&lt;meta name="DC.Title" content="'.$row_MetaTitle.'"&gt;<br />';
echo '&lt;meta name="DC.Creator" content="'.$row_MetaCreatorSort.'"&gt;<br />';
                            while($row = mysqli_fetch_array($DCSubjectsResult))
							{
							    $row_SubjectDescription = $row['SubjectDescription'];
								echo '&lt;meta name="DC.Subject" content="'.$row_SubjectDescription.'"&gt;<br />';
							}
                            
							$stripSummary = str_replace("<br />", " ", $row_Summary);
							$dcSummary = str_replace(array("\r", "\n"), '', $stripSummary);
							echo '&lt;meta name="DC.Description" content="'.$dcSummary.'"&gt;<br />';
                            echo '&lt;meta name="DC.Publisher" content="'.$row_MetaPublisher.'"&gt;<br />';
echo '&lt;meta name="DC.Date" content="'.$row_MetaDate.'"&gt;<br />';
echo '&lt;meta name="DC.Type" content="'.$row_MetaType.'"&gt;<br />';
echo '&lt;meta name="DC.Format" content="'.$row_MetaFormat.'"&gt;<br />';
echo '&lt;meta name="DC.Identifier" content="ISBN 10: '.$row_MetaISBN10.'"&gt;<br />';
echo '&lt;meta name="DC.Identifier" content="ISBN 13: '.$row_MetaISBN13.'"&gt;<br />';
							while($row = mysqli_fetch_array($DCLanguageResult))
							{
								$row_Language = $row['Language'];
								$row_ISOCode = $row['ISOCode'];
								echo '&lt;meta name="DC.Language" content="'.$row_Language.'"&gt;<br />';
								echo '&lt;meta name="DC.Language" content="'.$row_ISOCode.'"&gt;<br />';
							}
                        	echo '</code></pre>';
							?>
						</div>
						<div class="image">
							<img src="images/dublincore.png" alt="" />
						</div>
					</section>
				
				<!-- Four A -->
					<section class="spotlight style1 orient-left content-align-left image-position-center onscroll-image-fade-in">
						<div class="content">
							<h2>Basic MARC Record</h2>
							<?php
								while($row = mysqli_fetch_array($MARCResult))
								{
									$row_MARC = $row['MARC'];
									echo $row_MARC;
								}
							?>
							
						</div>
						<div class="image">
							<img src="images/marc.jpg" alt="" />
						</div>
					</section>

				<!-- Footer -->
					<footer class="wrapper style1 align-center">
						<div class="inner">
							<ul class="icons">
								<li><a href="#" class="icon brands style2 fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon brands style2 fa-facebook-f"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon brands style2 fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon brands style2 fa-linkedin-in"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon style2 fa-envelope"><span class="label">Email</span></a></li>
							</ul>
							<p>&copy; Creative Commons By-NC-SA.<br />Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
						</div>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>