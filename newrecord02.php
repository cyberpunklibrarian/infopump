<?php
include 'creds.php';

// Get and use an item record ID (ItemRecordID) to use for a data pull
$ItemID = htmlspecialchars($_GET["itemID"]);

?>

<!DOCTYPE html>
<html>
  <head>
    <title>Add New Record - Part Two - Subject Headings</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <style>
      html, body {
      min-height: 100%;
      }
      body, div, form, input, select, p { 
      padding: 0;
      margin: 0;
      outline: none;
      font-family: Roboto, Arial, sans-serif;
      font-size: 16px;
      color: #eee;
      }
      body {
      background: url("images/newitembackground.jpg") no-repeat center;
      background-size: cover;
      }
      h1, h2 {
      text-transform: uppercase;
      font-weight: 400;
      }
      h2 {
      margin: 0 0 0 8px;
      }
      .main-block {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      height: 100%;
      padding: 25px;
      background: rgba(0, 0, 0, 0.5); 
      }
      .left-part, form {
      padding: 25px;
      }
      .left-part {
      text-align: center;
      }
      .fa-graduation-cap {
      font-size: 72px;
      }
      form {
      background: rgba(0, 0, 0, 0.7); 
      }
      .title {
      display: flex;
      align-items: center;
      margin-bottom: 20px;
      }
      .info {
      display: flex;
      flex-direction: column;
      }
      input, select {
      padding: 5px;
      margin-bottom: 30px;
      background: transparent;
      border: none;
      border-bottom: 1px solid #eee;
      }
      input::placeholder {
      color: #eee;
      }
      option:focus {
      border: none;
      }
      option {
      background: black; 
      border: none;
      }
      .checkbox input {
      margin: 0 10px 0 0;
      vertical-align: middle;
      }
      .checkbox a {
      color: #26a9e0;
      }
      .checkbox a:hover {
      color: #85d6de;
      }
      .btn-item, button {
      padding: 10px 5px;
      margin-top: 20px;
      border-radius: 5px; 
      border: none;
      background: #26a9e0; 
      text-decoration: none;
      font-size: 15px;
      font-weight: 400;
      color: #fff;
      }
      .btn-item {
      display: inline-block;
      margin: 20px 5px 0;
      }
      button {
      width: 100%;
      }
      button:hover, .btn-item:hover {
      background: #85d6de;
      }
      @media (min-width: 568px) {
      html, body {
      height: 100%;
      }
      .main-block {
      flex-direction: row;
      height: calc(100% - 50px);
      }
      .left-part, form {
      flex: 1;
      height: auto;
      }
      }
    </style>
  </head>
  <body>
    <div class="main-block">
      <form action="<?php echo 'addsubjects.php?itemid='.$ItemID ?>" id="addnewrecord-subjects" method="post" enctype="multipart/form-data">
        <div class="title">
          <i class="fas fa-pencil-alt"></i>
          <h2>Add New Record - Part Two - Subject Headings</h2>
        </div>
        <div class="info">
          <select name = "subject01">
          <option value = "nosubject01" selected>Select up to five subject headings</option>          
            
            <?php

            $subjects = 'SELECT SubjectID, Description FROM SubjectHeadingIndex ORDER BY Description';

            $GetSubjects = mysqli_query($conn,$subjects);

            while($row = mysqli_fetch_array($GetSubjects)) {
                $row_SubjectID = $row['SubjectID'];
                $row_Description = $row['Description'];

                echo '<option value="'.$row_SubjectID.'">'.$row_Description.'</option>';

            }
            ?>
        </select>

        <select name = "subject02">
          <option value = "nosubject02" selected>Sebject Heading 02</option>          
            
            <?php

            $subjects = 'SELECT SubjectID, Description FROM SubjectHeadingIndex ORDER BY Description';

            $GetSubjects = mysqli_query($conn,$subjects);

            while($row = mysqli_fetch_array($GetSubjects)) {
                $row_SubjectID = $row['SubjectID'];
                $row_Description = $row['Description'];

                echo '<option value="'.$row_SubjectID.'">'.$row_Description.'</option>';

            }
            ?>
        </select>

        <select name = "subject03">
          <option value = "nosubject03" selected>Sebject Heading 03</option>          
            
            <?php

            $subjects = 'SELECT SubjectID, Description FROM SubjectHeadingIndex ORDER BY Description';

            $GetSubjects = mysqli_query($conn,$subjects);

            while($row = mysqli_fetch_array($GetSubjects)) {
                $row_SubjectID = $row['SubjectID'];
                $row_Description = $row['Description'];

                echo '<option value="'.$row_SubjectID.'">'.$row_Description.'</option>';

            }
            ?>
        </select>

        <select name = "subject04">
          <option value = "nosubject04" selected>Sebject Heading 04</option>          
            
            <?php

            $subjects = 'SELECT SubjectID, Description FROM SubjectHeadingIndex ORDER BY Description';

            $GetSubjects = mysqli_query($conn,$subjects);

            while($row = mysqli_fetch_array($GetSubjects)) {
                $row_SubjectID = $row['SubjectID'];
                $row_Description = $row['Description'];

                echo '<option value="'.$row_SubjectID.'">'.$row_Description.'</option>';

            }
            ?>
        </select>

        <select name = "subject05">
          <option value = "nosubject05" selected>Sebject Heading 05</option>          
            
            <?php

            $subjects = 'SELECT SubjectID, Description FROM SubjectHeadingIndex ORDER BY Description';

            $GetSubjects = mysqli_query($conn,$subjects);

            while($row = mysqli_fetch_array($GetSubjects)) {
                $row_SubjectID = $row['SubjectID'];
                $row_Description = $row['Description'];

                echo '<option value="'.$row_SubjectID.'">'.$row_Description.'</option>';

            }
            ?>
        </select>

        <input type="hidden" id="ItemID" name="ItemID" value="<?php echo $ItemID;?>">


        </div>
        <button type="submit" href="/">Submit and continue</button>
      </form>
    </div>
  </body>
</html>