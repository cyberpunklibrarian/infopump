<?php
include 'creds.php';

# Set variables
$subject01 = filter_var($_POST['subject01'], FILTER_SANITIZE_STRING);
$subject02 = filter_var($_POST['subject02'], FILTER_SANITIZE_STRING);
$subject03 = filter_var($_POST['subject03'], FILTER_SANITIZE_STRING);
$subject04 = filter_var($_POST['subject04'], FILTER_SANITIZE_STRING);
$subject05 = filter_var($_POST['subject05'], FILTER_SANITIZE_STRING);
$ItemID = filter_var($_POST['ItemID'], FILTER_SANITIZE_STRING);
$errNoSubjects = 0;

# Debugging output
echo 'ItemID: '.$ItemID.'<br />';
echo 'Selected Subject 01: '.$subject01.'<br />';
echo 'Selected Subject 02: '.$subject02.'<br />';
echo 'Selected Subject 03: '.$subject03.'<br />';
echo 'Selected Subject 04: '.$subject04.'<br />';
echo 'Selected Subject 05: '.$subject05.'<br /><br />';

# Run calculation to make sure the user selected at least one subject
if ($subject01 == 'nosubject01')
{
    $errNoSubjects = ++$errNoSubjects;
}

if ($subject02 == 'nosubject02')
{
    $errNoSubjects = ++$errNoSubjects;
}

if ($subject03 == 'nosubject03')
{
    $errNoSubjects = ++$errNoSubjects;
}

if ($subject04 == 'nosubject04')
{
    $errNoSubjects = ++$errNoSubjects;
}

if ($subject05 == 'nosubject05')
{
    $errNoSubjects = ++$errNoSubjects;
}

# If the user didn't select at least one subject, throw an error
if ($errNoSubjects == 5)
{
    echo '<h2>You must choose at least one subject.</h2>';
    echo "<html><meta http-equiv=\"refresh\" content=\"5;URL='newrecord02.php'\"></html>";

}

echo '$errNoSubjects: '.$errNoSubjects.'<br /><br />';

# Get subject codes for entry into the database

if ($subject01 != 'nosubject01')
{
    $GetSubject01 = "SELECT
    SubjectID,
    Description
    FROM SubjectHeadingIndex
    WHERE SubjectID = '$subject01'";

    $SetSubject01 = mysqli_query($conn,$GetSubject01);

    while($row = mysqli_fetch_array($SetSubject01)) {
        $row_Subject01ID = $row['SubjectID'];
        $row_Subject01Desc = $row['Description'];
        echo 'Subject01 ID: '.$row_Subject01ID.'<br />';
        echo 'Subject01 Desc: '.$row_Subject01Desc.'<br /><br />';
    }
}

if ($subject02 != 'nosubject02')
{
    $GetSubject02 = "SELECT
    SubjectID,
    Description
    FROM SubjectHeadingIndex
    WHERE SubjectID = '$subject02'";

    $SetSubject02 = mysqli_query($conn,$GetSubject02);

    while($row = mysqli_fetch_array($SetSubject02)) {
        $row_Subject02ID = $row['SubjectID'];
        $row_Subject02Desc = $row['Description'];
        echo 'Subject02 ID: '.$row_Subject02ID.'<br />';
        echo 'Subject02 Desc: '.$row_Subject02Desc.'<br /><br />';
    }
}

if ($subject03 != 'nosubject03')
{
    $GetSubject03 = "SELECT
    SubjectID,
    Description
    FROM SubjectHeadingIndex
    WHERE SubjectID = '$subject03'";

    $SetSubject03 = mysqli_query($conn,$GetSubject03);

    while($row = mysqli_fetch_array($SetSubject03)) {
        $row_Subject03ID = $row['SubjectID'];
        $row_Subject03Desc = $row['Description'];
        echo 'Subject03 ID: '.$row_Subject03ID.'<br />';
        echo 'Subject03 Desc: '.$row_Subject03Desc.'<br /><br />';
    }
}

if ($subject04 != 'nosubject04')
{
    $GetSubject04 = "SELECT
    SubjectID,
    Description
    FROM SubjectHeadingIndex
    WHERE SubjectID = '$subject04'";

    $SetSubject04 = mysqli_query($conn,$GetSubject04);

    while($row = mysqli_fetch_array($SetSubject04)) {
        $row_Subject04ID = $row['SubjectID'];
        $row_Subject04Desc = $row['Description'];
        echo 'Subject04 ID: '.$row_Subject04ID.'<br />';
        echo 'Subject04 Desc: '.$row_Subject04Desc.'<br /><br />';
    }
}

if ($subject05 != 'nosubject05')
{
    $GetSubject05 = "SELECT
    SubjectID,
    Description
    FROM SubjectHeadingIndex
    WHERE SubjectID = '$subject05'";

    $SetSubject05 = mysqli_query($conn,$GetSubject05);

    while($row = mysqli_fetch_array($SetSubject05)) {
        $row_Subject05ID = $row['SubjectID'];
        $row_Subject05Desc = $row['Description'];
        echo 'Subject05 ID: '.$row_Subject05ID.'<br />';
        echo 'Subject05 Desc: '.$row_Subject05Desc.'<br /><br />';
    }
}

# Update the database with subject headings

if ($subject01 != 'nosubject01')
{
    $QueryUpdateSubject01 = "INSERT INTO ItemSubjects
    (ItemRecordID, SubjectID)
    VALUES
    ('$ItemID', '$row_Subject01ID')";
    
    if (mysqli_query($conn, $QueryUpdateSubject01)) {
        echo '<strong>Database updated: Subject 01</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

if ($subject02 != 'nosubject02')
{
    $QueryUpdateSubject02 = "INSERT INTO ItemSubjects
    (ItemRecordID, SubjectID)
    VALUES
    ('$ItemID', '$row_Subject02ID')";
    
    if (mysqli_query($conn, $QueryUpdateSubject02)) {
        echo '<strong>Database updated: Subject 02</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

if ($subject03 != 'nosubject03')
{
    $QueryUpdateSubject03 = "INSERT INTO ItemSubjects
    (ItemRecordID, SubjectID)
    VALUES
    ('$ItemID', '$row_Subject03ID')";
    
    if (mysqli_query($conn, $QueryUpdateSubject03)) {
        echo '<strong>Database updated: Subject 03</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

if ($subject04 != 'nosubject04')
{
    $QueryUpdateSubject04 = "INSERT INTO ItemSubjects
    (ItemRecordID, SubjectID)
    VALUES
    ('$ItemID', '$row_Subject04ID')";
    
    if (mysqli_query($conn, $QueryUpdateSubject04)) {
        echo '<strong>Database updated: Subject 04</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

if ($subject05 != 'nosubject05')
{
    $QueryUpdateSubject05 = "INSERT INTO ItemSubjects
    (ItemRecordID, SubjectID)
    VALUES
    ('$ItemID', '$row_Subject05ID')";
    
    if (mysqli_query($conn, $QueryUpdateSubject05)) {
        echo '<strong>Database updated: Subject 05</strong><br />';
    } else {
        echo 'Error: '.$sql.'<br />'.mysqli_error($conn);
    }
}

header("Location: newrecord03.php?itemID=$ItemID");

?>