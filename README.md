# Infopump
## A personal bibliographic presentation system.

### Under Development - This software is nowhere near ready for use.

Infopump is a basic content management system to present books and other media in a way that makes the information finable and shareable. With an eye towards librarians, information professionals, and library users; Infopump offers a way to share a collection with others so they can benefit from your curation and discover new materials. 

Infopump is a free and open source project running on:

* Linux
* Apache
* MariaDB
* PHP
* HTML5

### Credits

I'd like to thank HTML5 UP for the Story template which is used in the front end presentation of Infopump. They did some beautiful work, and you should check out their website for more eye candy.

* [HTML5 UP](https://html5up.net/)
* [Story template](https://html5up.net/story)