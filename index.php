<?php
include 'creds.php';

$Featured = "SELECT
    ir.ItemRecordID AS ItemRecordID,
    ir.CreatorBrowse AS CreatorBrowse,
    ir.Title AS Title,
    ir.Format AS ItemFormat,
    ici.FileName AS CoverFile,
    ici.AltText AS CoverAltText
    FROM ItemRecords ir
    INNER JOIN ItemCoverIndex ici ON ici.ItemCoverID = ir.ItemRecordID
    WHERE CreationDate >= (DATE(NOW()) - INTERVAL 365 DAY)
    LIMIT 4";

$FeaturedResult = mysqli_query($conn, $Featured);
?>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Infopump - Cyberpunk Culture Database</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Infopump: A bibliographic display system" />
	<meta name="keywords" content="free software, open source, bibliography, cyberpunk, books, media, movies, video games" />
	<meta name="author" content="Daniel Messer" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="Infopump"/>
	<meta property="og:image" content="https://i.imgur.com/LGdE8aW.jpg"/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content="Infopump: A bibliographic display system"/>
	<meta property="og:description" content="A bibliographic display system"/>
	<meta name="twitter:title" content="Infopump: A bibliographic display system" />
	<meta name="twitter:image" content="https://i.imgur.com/LGdE8aW.jpg" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic|Roboto:400,300,700' rel='stylesheet' type='text/css'>
	<!-- Animate -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon -->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/style.css">


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<div id="fh5co-offcanvas">
		<a href="#" class="fh5co-close-offcanvas js-fh5co-close-offcanvas"><span><i class="icon-cross3"></i> <span>Close</span></span></a>
		<div class="fh5co-bio">
			<figure>
				<img src="images/avatar.jpg" alt="Infopump Avatar" class="img-responsive">
			</figure>
			<h3 class="heading">About the Project</h3>
			<h2>Infopump</h2>
			<p>A bibliographic management and display system.</p>
			<ul class="fh5co-social">
				<li><a href="#"><i class="icon-twitter"></i></a></li>
				<!--<li><a href="#"><i class="icon-facebook"></i></a></li>
				<li><a href="#"><i class="icon-instagram"></i></a></li>-->
			</ul>
		</div>

		<div class="fh5co-menu">
			<div class="fh5co-box">
				<h3 class="heading">Categories</h3>
				<ul>
					<li><a href="#">Books</a></li>
					<li><a href="#">Movies</a></li>
					<li><a href="#">Television</a></li>
					<li><a href="#">Video Games</a></li>
					<li><a href="#">Other</a></li>
				</ul>
			</div>
			<div class="fh5co-box">
				<h3 class="heading">Search</h3>
				<form action="#">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="This doesn't work yet">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- END #fh5co-offcanvas -->
	<header id="fh5co-header">
		
		<div class="container-fluid">

			<div class="row">
				<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
				<ul class="fh5co-social">
					<li><a href="#"><i class="icon-twitter"></i></a></li>
					<!--<li><a href="#"><i class="icon-facebook"></i></a></li>
					<li><a href="#"><i class="icon-instagram"></i></a></li>-->
				</ul>
				<div class="col-lg-12 col-md-12 text-center">
					<h1 id="fh5co-logo"><a href="index.php">Infopump - Cyberpunk Culture Database</a></h1>
				</div>

			</div>
		
		</div>

	</header>
	<!-- END #fh5co-header -->

    <!-- BEGIN Featured Covers -->
    <div class="container-fluid">
        <div class="row fh5co-post-entry">
        
        <?php
        while($row = mysqli_fetch_array($FeaturedResult))
        {
            $row_ItemRecordID = $row['ItemRecordID'];
            $row_CreatorBrowse = $row['CreatorBrowse'];
            $row_Title = $row['Title'];
            $row_ItemFormat = $row['ItemFormat'];
            $row_CoverFile = $row['CoverFile'];
            $row_CoverAltText = $row['CoverAltText'];

            echo '<article class="col-lg-3 col-md-3 col-sm-3 col-xs-6 col-xxs-12 animate-box">';
                echo '<figure>';
			        echo '<a href="itemrecord.php?itemid='.$row_ItemRecordID.'"><img src="images/'.$row_CoverFile.'" alt="'.$row_CoverAltText.'" title="'.$row_CoverAltText.'" class="img-responsive"></a>';
		        echo '</figure>';
                echo '<span class="fh5co-meta">'.$row_ItemFormat.'</span>';
				echo '<h2 class="fh5co-article-title"><a href="single.html">'.$row_Title.'</a></h2>';
				echo '<span class="fh5co-meta fh5co-date">'.$row_CreatorBrowse.'</span>';
            echo '</article>';
        }       
        ?>
        </div>
    </div>
    <!-- END Featured Covers -->

	<footer id="fh5co-footer">
		<p><small>&copy; Creative Commons By-NC-SA<br> Design by <a href="http://freehtml5.co" target="_blank">FREEHTML5.co</a></small></p>
	</footer>


	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Main JS -->
	<script src="js/main.js"></script>

	</body>
</html>

