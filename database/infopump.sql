-- Adminer 4.8.1 MySQL 5.5.5-10.3.29-MariaDB-0ubuntu0.20.04.1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `FurtherInfoIndex`;
CREATE TABLE `FurtherInfoIndex` (
  `FurtherInfoID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(250) NOT NULL,
  PRIMARY KEY (`FurtherInfoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Provides index for FurtherInfo applied to item records';


DROP TABLE IF EXISTS `ItemCoverIndex`;
CREATE TABLE `ItemCoverIndex` (
  `ItemCoverID` int(11) NOT NULL,
  `FileName` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index of the cover images to be displayed with item records';


DROP TABLE IF EXISTS `ItemCovers`;
CREATE TABLE `ItemCovers` (
  `ItemRecordID` int(11) NOT NULL,
  `ItemCoverID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cover image information for item records';


DROP TABLE IF EXISTS `ItemFurtherInfo`;
CREATE TABLE `ItemFurtherInfo` (
  `ItemRecordID` int(11) NOT NULL,
  `FurtherInfoID` int(11) NOT NULL,
  `Content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Further info as appearing on item records';


DROP TABLE IF EXISTS `ItemLanguages`;
CREATE TABLE `ItemLanguages` (
  `ItemRecordID` int(11) NOT NULL,
  `LanguageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Refer to LanguageIndex table';


DROP TABLE IF EXISTS `ItemRecords`;
CREATE TABLE `ItemRecords` (
  `ItemRecordID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  `CreatorSort` varchar(255) NOT NULL,
  `CreatorBrowse` varchar(255) NOT NULL,
  `Publisher` varchar(255) DEFAULT NULL,
  `Date` year(4) DEFAULT NULL,
  `Type` tinytext NOT NULL,
  `Format` tinytext NOT NULL,
  `ISBN10` varchar(20) DEFAULT NULL,
  `ISBN13` varchar(20) DEFAULT NULL,
  `OCLC` varchar(20) DEFAULT NULL,
  `UPC` varchar(20) DEFAULT NULL,
  `DOI` varchar(20) DEFAULT NULL,
  `ISSN` varchar(20) DEFAULT NULL,
  `CreationDate` datetime NOT NULL,
  `ModifiedDate` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ItemRecordID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The basic item data';


SET NAMES utf8mb4;

DROP TABLE IF EXISTS `ItemSeries`;
CREATE TABLE `ItemSeries` (
  `ItemRecordID` int(11) NOT NULL,
  `SeriesID` int(11) NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `ItemSubjects`;
CREATE TABLE `ItemSubjects` (
  `ItemRecordID` int(11) NOT NULL,
  `SubjectID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Refers to SubjectHeadingIndex';


DROP TABLE IF EXISTS `LanguageIndex`;
CREATE TABLE `LanguageIndex` (
  `LanguageID` int(11) NOT NULL AUTO_INCREMENT,
  `ISOCode` varchar(250) NOT NULL,
  `Description` varchar(250) NOT NULL,
  PRIMARY KEY (`LanguageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index of languages to be applied to item records';


DROP TABLE IF EXISTS `MARCRecords`;
CREATE TABLE `MARCRecords` (
  `ItemRecordID` int(11) NOT NULL,
  `MARC` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Basic MARC record data displayed with item records.';


DROP TABLE IF EXISTS `SeriesIndex`;
CREATE TABLE `SeriesIndex` (
  `SeriesID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(250) NOT NULL,
  `SeriesSlug` varchar(250) NOT NULL,
  PRIMARY KEY (`SeriesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `SubjectHeadingIndex`;
CREATE TABLE `SubjectHeadingIndex` (
  `SubjectID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(250) NOT NULL,
  PRIMARY KEY (`SubjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index of subject headings to be applied to item records';


DROP TABLE IF EXISTS `Summaries`;
CREATE TABLE `Summaries` (
  `ItemRecordID` int(11) NOT NULL,
  `Summary` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Item summaries that will be displayed with item records';


DROP TABLE IF EXISTS `TypeIndex`;
CREATE TABLE `TypeIndex` (
  `TypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(250) NOT NULL,
  PRIMARY KEY (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2021-06-28 16:15:06