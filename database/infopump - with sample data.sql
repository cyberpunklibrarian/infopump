-- Adminer 4.8.1 MySQL 5.5.5-10.3.29-MariaDB-0ubuntu0.20.04.1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `FurtherInfoIndex`;
CREATE TABLE `FurtherInfoIndex` (
  `FurtherInfoID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(250) NOT NULL,
  PRIMARY KEY (`FurtherInfoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Provides index for FurtherInfo applied to item records';

INSERT INTO `FurtherInfoIndex` (`FurtherInfoID`, `Description`) VALUES
(1,	'WorldCat'),
(2,	'Goodreads');

DROP TABLE IF EXISTS `ItemCoverIndex`;
CREATE TABLE `ItemCoverIndex` (
  `ItemCoverID` int(11) NOT NULL,
  `FileName` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index of the cover images to be displayed with item records';


DROP TABLE IF EXISTS `ItemCovers`;
CREATE TABLE `ItemCovers` (
  `ItemRecordID` int(11) NOT NULL,
  `ItemCoverID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cover image information for item records';


DROP TABLE IF EXISTS `ItemFurtherInfo`;
CREATE TABLE `ItemFurtherInfo` (
  `ItemRecordID` int(11) NOT NULL,
  `FurtherInfoID` int(11) NOT NULL,
  `Content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Further info as appearing on item records';

INSERT INTO `ItemFurtherInfo` (`ItemRecordID`, `FurtherInfoID`, `Content`) VALUES
(1,	1,	'https://www.worldcat.org/title/neuromancer/oclc/1240435965'),
(1,	2,	'https://www.goodreads.com/book/show/6088007-neuromancer');

DROP TABLE IF EXISTS `ItemLanguages`;
CREATE TABLE `ItemLanguages` (
  `ItemRecordID` int(11) NOT NULL,
  `LanguageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Refer to LanguageIndex table';

INSERT INTO `ItemLanguages` (`ItemRecordID`, `LanguageID`) VALUES
(1,	1);

DROP TABLE IF EXISTS `ItemRecords`;
CREATE TABLE `ItemRecords` (
  `ItemRecordID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  `CreatorSort` varchar(255) NOT NULL,
  `CreatorBrowse` varchar(255) NOT NULL,
  `Publisher` varchar(255) NOT NULL,
  `Date` year(4) NOT NULL,
  `Type` tinytext NOT NULL,
  `Format` tinytext NOT NULL,
  `ISBN10` varchar(20) NOT NULL,
  `ISBN13` varchar(20) NOT NULL,
  `CreationDate` datetime NOT NULL,
  `ModifiedDate` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ItemRecordID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The basic item data';

INSERT INTO `ItemRecords` (`ItemRecordID`, `Title`, `CreatorSort`, `CreatorBrowse`, `Publisher`, `Date`, `Type`, `Format`, `ISBN10`, `ISBN13`, `CreationDate`, `ModifiedDate`) VALUES
(1,	'Neuromancer',	'Gibson, William',	'William Gibson',	'Ace',	'1984',	'text',	'book',	'0441569560',	'9780441569560',	'2021-05-29 12:02:35',	'2021-05-29 12:02:35'),
(2,	'Mirrorshades',	'Sterling, Bruce',	'Bruce Sterling',	'Arbor House',	'1986',	'text',	'book',	'0877958680',	'9780877958680',	'2021-05-30 10:23:51',	'2021-05-30 10:23:51');

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `ItemSeries`;
CREATE TABLE `ItemSeries` (
  `ItemRecordID` int(11) NOT NULL,
  `SeriesID` int(11) NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `ItemSeries` (`ItemRecordID`, `SeriesID`, `Description`) VALUES
(1,	2,	'Book 1'),
(2,	2,	'Not part of a series');

DROP TABLE IF EXISTS `ItemSubjects`;
CREATE TABLE `ItemSubjects` (
  `ItemRecordID` int(11) NOT NULL,
  `SubjectID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Refers to SubjectHeadingIndex';

INSERT INTO `ItemSubjects` (`ItemRecordID`, `SubjectID`) VALUES
(1,	1),
(1,	2),
(1,	3);

DROP TABLE IF EXISTS `LanguageIndex`;
CREATE TABLE `LanguageIndex` (
  `LanguageID` int(11) NOT NULL AUTO_INCREMENT,
  `ISOCode` varchar(250) NOT NULL,
  `Description` varchar(250) NOT NULL,
  PRIMARY KEY (`LanguageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index of languages to be applied to item records';

INSERT INTO `LanguageIndex` (`LanguageID`, `ISOCode`, `Description`) VALUES
(1,	'en-us',	'English (United States)'),
(2,	'fr',	'French'),
(3,	'fr-ca',	'French (Canada)'),
(4,	'de',	'German'),
(5,	'es',	'Spanish'),
(6,	'es-mx',	'Spanish (Mexico)'),
(7,	'ja',	'Japanese'),
(8,	'eo',	'Esperanto');

DROP TABLE IF EXISTS `MARCRecords`;
CREATE TABLE `MARCRecords` (
  `ItemRecordID` int(11) NOT NULL,
  `MARC` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Basic MARC record data displayed with item records.';


DROP TABLE IF EXISTS `SeriesIndex`;
CREATE TABLE `SeriesIndex` (
  `SeriesID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(250) NOT NULL,
  `SeriesSlug` varchar(250) NOT NULL,
  PRIMARY KEY (`SeriesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `SeriesIndex` (`SeriesID`, `Description`, `SeriesSlug`) VALUES
(1,	'Singular Work',	'none'),
(2,	'Sprawl Trilogy',	'gibsonsprawltrilogy'),
(3,	'Bridge Trilogy',	'gibsonbridgetrilogy'),
(4,	'Blue Ant Trilogy',	'gibsonblueanttrilogy');

DROP TABLE IF EXISTS `SubjectHeadingIndex`;
CREATE TABLE `SubjectHeadingIndex` (
  `SubjectID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(250) NOT NULL,
  PRIMARY KEY (`SubjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index of subject headings to be applied to item records';

INSERT INTO `SubjectHeadingIndex` (`SubjectID`, `Description`) VALUES
(1,	'Hackers -- Fiction'),
(2,	'Genre -- Cyberpunk fiction'),
(3,	'Genre -- Science Fiction');

DROP TABLE IF EXISTS `Summaries`;
CREATE TABLE `Summaries` (
  `ItemRecordID` int(11) NOT NULL,
  `Summary` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Item summaries that will be displayed with item records';

INSERT INTO `Summaries` (`ItemRecordID`, `Summary`) VALUES
(1,	'<p>Case was the sharpest data-thief in the matrix -- until he crossed the wrong people and they crippled his nervous system, banishing him from cyberspace. Now a mysterious new employer has recruited him for a last-chance run at an unthinkably powerful artificial intelligence. With a dead man riding shotgun and Molly, a mirror-eyed street-samurai, to watch his back, Case is ready for the adventure that upped the ante on an entire genre of fiction. </p>\r\n<p>Neuromancer was the first fully-realized glimpse of humankind\'s digital future--a shocking vision that has challenged our assumptions about technology and ourselves, reinvented the way we speak and think, and forever altered the landscape of our imaginations.</p>');

DROP TABLE IF EXISTS `TypeIndex`;
CREATE TABLE `TypeIndex` (
  `TypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(250) NOT NULL,
  PRIMARY KEY (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `TypeIndex` (`TypeID`, `Description`) VALUES
(1,	'Collection'),
(2,	'Dataset'),
(3,	'Event'),
(4,	'Image'),
(5,	'InteractiveResource'),
(6,	'MovingImage'),
(7,	'PhysicalObject'),
(8,	'Service'),
(9,	'Software'),
(10,	'Sound'),
(11,	'StillImage'),
(12,	'Text');

-- 2021-06-21 21:47:56
