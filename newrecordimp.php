<!DOCTYPE html>
<!--[if lt IE 7]>      
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>         
<html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>         
<html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Infopump - Cyberpunk Culture Database</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Infopump: A bibliographic display system" />
	<meta name="keywords" content="free software, open source, bibliography, cyberpunk, books, media, movies, video games" />
	<meta name="author" content="Daniel Messer" />
	<!-- 
                  //////////////////////////////////////////////////////
                  
                  FREE HTML5 TEMPLATE 
                  DESIGNED & DEVELOPED by FREEHTML5.CO
                  	
                  Website: 		http://freehtml5.co/
                  Email: 			info@freehtml5.co
                  Twitter: 		http://twitter.com/fh5co
                  Facebook: 		https://www.facebook.com/fh5co
                  
                  //////////////////////////////////////////////////////
                   -->
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic|Roboto:400,300,700' rel='stylesheet' type='text/css'>
	<!-- Animate -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon -->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<!--<link rel="stylesheet" href="css/bootstrap.css">-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Tooltip CSS -->
	<link href="css/tooltips.css" rel="stylesheet">
	<!-- Friconix -->
	<script defer src="https://friconix.com/cdn/friconix.js">
	</script>
	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
               <script src="js/respond.min.js"></script>
               <![endif]-->
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="jumbotron jumbotron-fluid">
					<div class="" style="padding: 0 2em 0 2em;">
						<h2>New Item Record</h2> </div>
					<div class="rendered-form" style="padding: 0 2em 0 2em;">
						<!-- BEING BASIC ITEM INFORMATION -->
						<hr>
						<div class="">
							<h3 access="false" id="control-3080028">Basic Item Information</h3> </div>
						<div class="">
							<p access="false" id="control-8303991">The underlying information for this item. Fill out these fields as completely as possible.</p>
						</div>
						<div class="formbuilder-text form-group field-text-1644202850812">
							<label for="text-1644202850812" class="formbuilder-text-label">Title<span class="formbuilder-required">*</span><span data-tooltip="The primary title for this item" data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<input type="text" placeholder="Item Title - Required" class="form-control" name="text-1644202850812" access="false" id="text-1644202850812" title="The item's primary title" required="required" aria-required="true"> </div>
						<div class="formbuilder-text form-group field-text-1644202854913">
							<label for="text-1644202854913" class="formbuilder-text-label">Creator | Author | Director<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<input type="text" placeholder="Lastname, Firstname" class="form-control" name="text-1644202854913" access="false" id="text-1644202854913" title="The item's primary creator"> </div>
						<div class="formbuilder-text form-group field-text-1644202862749">
							<label for="text-1644202862749" class="formbuilder-text-label">Publisher<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<input type="text" class="form-control" name="text-1644202862749" access="false" id="text-1644202862749" title="The publisher or distributor for this item"> </div>
						<div class="formbuilder-text form-group field-text-1644202864349">
							<label for="text-1644202864349" class="formbuilder-text-label">Publication Date<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<input type="text" class="form-control" name="text-1644202864349" access="false" id="text-1644202864349" title="This can be a full date or only a year."> </div>
						<div class="formbuilder-select form-group field-select-1644202882337">
							<label for="select-1644202882337" class="formbuilder-select-label">Series<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<select class="form-control" name="select-1644202882337" id="select-1644202882337">
								<option value="option-1" selected="true" id="select-1644202882337-0">Option 1</option>
								<option value="option-2" id="select-1644202882337-1">Option 2</option>
								<option value="option-3" id="select-1644202882337-2">Option 3</option>
							</select>
						</div>
						<div class="formbuilder-number form-group field-number-1644202899708">
							<label for="number-1644202899708" class="formbuilder-number-label">Series Position<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<input type="number" class="form-control" name="number-1644202899708" access="false" id="number-1644202899708" title="The ordinal position in the series above. Leave blank if not part of a series."> </div>
						<div class="formbuilder-select form-group field-select-1644202919815">
							<label for="select-1644202919815" class="formbuilder-select-label">Item Type<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<select class="form-control" name="select-1644202919815" id="select-1644202919815">
								<option value="option-1" selected="true" id="select-1644202919815-0">Option 1</option>
								<option value="option-2" id="select-1644202919815-1">Option 2</option>
								<option value="option-3" id="select-1644202919815-2">Option 3</option>
							</select>
						</div>
						<div class="formbuilder-text form-group field-text-1644202944586">
							<label for="text-1644202944586" class="formbuilder-text-label">Format<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<input type="text" class="form-control" name="text-1644202944586" access="false" id="text-1644202944586" title="Enter a format if applicable"> </div>
						<div class="formbuilder-select form-group field-select-1644202949656">
							<label for="select-1644202949656" class="formbuilder-select-label">Language<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
							<select class="form-control" name="select-1644202949656" id="select-1644202949656">
								<option value="option-1" selected="true" id="select-1644202949656-0">Option 1</option>
								<option value="option-2" id="select-1644202949656-1">Option 2</option>
								<option value="option-3" id="select-1644202949656-2">Option 3</option>
							</select>
						</div>
						<!-- END BASIC ITEM INFORMATION -->

						<!-- BEGIN SUMMARY -->
						<hr>
                        <div class="">
							<h3 access="false" id="control-3996331">Summary</h3> </div>
						<div class="">
							<p access="false" id="control-4628764">Provide a brief summary of the item's content.</p>
						</div>
						<div class="formbuilder-textarea form-group field-textarea-1644204040844">
							<label for="textarea-1644204040844" class="formbuilder-textarea-label">Text Area</label>
							<textarea type="textarea" class="form-control" name="textarea-1644204040844" access="false" rows="10" id="textarea-1644204040844"></textarea>
						</div>
						<!-- END SUMMARY -->

					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="jumbotron jumbotron-fluid" style="padding: 2em 2em 2em 2em;">
							<!-- BEGIN UNIQUE IDENTIFIERS -->
							<div class="">
								<h3 access="false" id="control-4141948">Unique Identifiers</h3> </div>
							<div class="">
								<p access="false" id="control-6975700">Specific and unique identifying information tied to the item.</p>
							</div>
							<div class="form-group">
								<label for="text-1644204181152" class="formbuilder-text-label">ISBN 10<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
								<input type="text" placeholder="1234567890" class="form-control" name="text-1644204181152" access="false" id="text-1644204181152" title="Enter the 10 digit ISBN without dashes or spaces"> </div>
							<div class="formbuilder-text form-group field-text-1644204183651">
								<label for="text-1644204183651" class="formbuilder-text-label">ISBN 13<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
								<input type="text" placeholder="1234567890123" class="form-control" name="text-1644204183651" access="false" id="text-1644204183651" title="Enter the13 digit ISBN without dashes or spaces"> </div>
							<div class="formbuilder-text form-group field-text-1644204185802">
								<label for="text-1644204185802" class="formbuilder-text-label">OCLC Number<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
								<input type="text" class="form-control" name="text-1644204185802" access="false" id="text-1644204185802" title="Enter the OCLC number for this item"> </div>
							<div class="formbuilder-text form-group field-text-1644204187986">
								<label for="text-1644204187986" class="formbuilder-text-label">ISSN<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
								<input type="text" placeholder="1234567890" class="form-control" name="text-1644204187986" access="false" id="text-1644204187986" title="Enter the ISSN without spaces or dashes"> </div>
							<div class="formbuilder-text form-group field-text-1644204189804">
								<label for="text-1644204189804" class="formbuilder-text-label">UPC<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
								<input type="text" placeholder="123456789" class="form-control" name="text-1644204189804" access="false" id="text-1644204189804" title="Enter the UPC number without dashes or spaces"> </div>
							<div class="formbuilder-text form-group field-text-1644204191803">
								<label for="text-1644204191803" class="formbuilder-text-label">DOI<span data-tooltip="Custom tooltip on the right." data-tooltip-position="right"><i class="fi-cnsuxm-question-mark"></i></span></label>
								<input type="text" placeholder="10.1080/15588742.2015.1017687" class="form-control" name="text-1644204191803" access="false" id="text-1644204191803" title="Enter only the DOI number without the URL information."> </div>
						</div>
						<!-- END UNIQUE IDENTIFIERS -->
					</div>
					<div class="col-md-6">
						<div class="jumbotron jumbotron-fluid"  style="padding: 2em 2em 2em 2em;">
						<!-- BEGIN FURTHER INFORMATION -->
						<div class="">
							<h3 access="false" id="control-2013465">Further Information</h3> </div>
						<div class="">
							<p access="false" id="control-5592131">Provide links to further information about the item. You may add up to five links.</p>
						</div>
						<div class="formbuilder-select form-group field-select-1644204693211">
							<label for="select-1644204693211" class="formbuilder-select-label">Further Info 1</label>
							<select class="form-control" name="select-1644204693211" id="select-1644204693211">
								<option value="option-1" selected="true" id="select-1644204693211-0">Option 1</option>
								<option value="option-2" id="select-1644204693211-1">Option 2</option>
								<option value="option-3" id="select-1644204693211-2">Option 3</option>
							</select>
						</div>
						<div class="formbuilder-text form-group field-text-1644204695614">
							<label for="text-1644204695614" class="formbuilder-text-label">Further Info 1 Content</label>
							<input type="text" class="form-control" name="text-1644204695614" access="false" id="text-1644204695614"> </div>
						<div class="formbuilder-select form-group field-select-1644205160715">
							<label for="select-1644205160715" class="formbuilder-select-label">Further Info 2</label>
							<select class="form-control" name="select-1644205160715" id="select-1644205160715">
								<option value="option-1" selected="true" id="select-1644205160715-0">Option 1</option>
								<option value="option-2" id="select-1644205160715-1">Option 2</option>
								<option value="option-3" id="select-1644205160715-2">Option 3</option>
							</select>
						</div>
						<div class="formbuilder-text form-group field-text-1644205148614">
							<label for="text-1644205148614" class="formbuilder-text-label">Further Info 2 Content</label>
							<input type="text" class="form-control" name="text-1644205148614" access="false" id="text-1644205148614"> </div>
						<div class="formbuilder-select form-group field-select-1644205164383">
							<label for="select-1644205164383" class="formbuilder-select-label">Further Info 3</label>
							<select class="form-control" name="select-1644205164383" id="select-1644205164383">
								<option value="option-1" selected="true" id="select-1644205164383-0">Option 1</option>
								<option value="option-2" id="select-1644205164383-1">Option 2</option>
								<option value="option-3" id="select-1644205164383-2">Option 3</option>
							</select>
						</div>
						<div class="formbuilder-text form-group field-text-1644205150995">
							<label for="text-1644205150995" class="formbuilder-text-label">Further Info 3 Content</label>
							<input type="text" class="form-control" name="text-1644205150995" access="false" id="text-1644205150995"> </div>
						<div class="formbuilder-select form-group field-select-1644205172502">
							<label for="select-1644205172502" class="formbuilder-select-label">Further Info 4</label>
							<select class="form-control" name="select-1644205172502" id="select-1644205172502">
								<option value="option-1" selected="true" id="select-1644205172502-0">Option 1</option>
								<option value="option-2" id="select-1644205172502-1">Option 2</option>
								<option value="option-3" id="select-1644205172502-2">Option 3</option>
							</select>
						</div>
						<div class="formbuilder-text form-group field-text-1644205153447">
							<label for="text-1644205153447" class="formbuilder-text-label">Further Info 4 Content</label>
							<input type="text" class="form-control" name="text-1644205153447" access="false" id="text-1644205153447"> </div>
						<div class="formbuilder-select form-group field-select-1644205174537">
							<label for="select-1644205174537" class="formbuilder-select-label">Further Info 5</label>
							<select class="form-control" name="select-1644205174537" id="select-1644205174537">
								<option value="option-1" selected="true" id="select-1644205174537-0">Option 1</option>
								<option value="option-2" id="select-1644205174537-1">Option 2</option>
								<option value="option-3" id="select-1644205174537-2">Option 3</option>
							</select>
						</div>
						<div class="formbuilder-text form-group field-text-1644205156881">
							<label for="text-1644205156881" class="formbuilder-text-label">Further Info 5 Content</label>
							<input type="text" class="form-control" name="text-1644205156881" access="false" id="text-1644205156881"> </div>
						<!-- END FURTHER INFORMATION -->
						</p>
                        </div>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <div class="jumbotron jumbotron-fluid"  style="padding: 2em 2em 2em 2em;">
                <!-- BEGIN SUBJECT HEADINGS -->
                        <div class="">          
							<h3 access="false" id="control-8361442">Subject Headings</h3> </div>
						<div class="">
							<p access="false" id="control-4700751">These are the overarching subject headings for the item. For best results, use standard Library of Congress Subject Headings. You may enter up to five different subject headings.</p>
						</div>
						<div class="formbuilder-text form-group field-text-1644203582079">
							<label for="text-1644203582079" class="formbuilder-text-label">Subject Heading 1</label>
							<input type="text" class="form-control" name="text-1644203582079" access="false" id="text-1644203582079"> </div>
						<div class="formbuilder-text form-group field-text-1644203589813">
							<label for="text-1644203589813" class="formbuilder-text-label">Subject Heading 2</label>
							<input type="text" class="form-control" name="text-1644203589813" access="false" id="text-1644203589813"> </div>
						<div class="formbuilder-text form-group field-text-1644203591999">
							<label for="text-1644203591999" class="formbuilder-text-label">Subject Heading 3</label>
							<input type="text" class="form-control" name="text-1644203591999" access="false" id="text-1644203591999"> </div>
						<div class="formbuilder-text form-group field-text-1644203584644">
							<label for="text-1644203584644" class="formbuilder-text-label">Subject Heading 4</label>
							<input type="text" class="form-control" name="text-1644203584644" access="false" id="text-1644203584644"> </div>
						<div class="formbuilder-text form-group field-text-1644203587196">
							<label for="text-1644203587196" class="formbuilder-text-label">Subject Heading 5</label>
							<input type="text" class="form-control" name="text-1644203587196" access="false" id="text-1644203587196"> </div>
						<!-- END SUBJECT HEADINGS -->

                        <!-- BEGIN COVER IMAGE AND SUBMIT -->
                        <hr>
						<div class="">
							<h3 access="false" id="control-3355394">Cover Image</h3> </div>
						<div class="">
							<p access="false" id="control-4011444">Upload a cover image to apply to the item record.</p>
						</div>
						<div class="formbuilder-file form-group field-file-1644205674332">
							<label for="file-1644205674332" class="formbuilder-file-label">Cover Image - Upload</label>
							<input type="file" class="form-control" name="file-1644205674332" access="false" multiple="false" id="file-1644205674332"> </div>
						<div class="formbuilder-button form-group field-button-1644205683283">
							<button type="submit" class="btn-primary btn" name="button-1644205683283" access="false" style="primary" id="button-1644205683283">Submit Item Record</button>
						</div>
                        <!-- END COVER IMAGE AND SUBMIT -->
            </div>
            <div class="col-md-2"></div>
        </div>
	</div>
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Main JS -->
	<script src="js/main.js"></script>
</body>

</html>